CUR_PATH := device/rockchip/common/app
PRODUCT_PACKAGES += \
	Chrome\
	MediaFloat \
	RkApkinstaller \
	RkExplorer \
    RkVideoPlayer
PRODUCT_COPY_FILES += \
	$(CUR_PATH)/apk/flashplayer:system/app/flashplayer 
