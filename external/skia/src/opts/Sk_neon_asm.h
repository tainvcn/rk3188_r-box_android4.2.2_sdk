#ifndef SK_NEON_ASM
#define SK_NEON_ASM

.macro default_init_neon_regs
	vpush	{d8-d15}
.endm

.macro default_cleanup_neon_regs
	vpop	{d8-d15}
.endm

#endif
