#ifndef __BT_CUSTMIZE__
#define __BT_CUSTMIZE__

#ifdef __cplusplus
extern "C" {
#endif

extern char * GetDisplayLable();
extern char * GetFwFullDir();
extern char * GetFwFilename();
extern void NotifyChipID( char * chip_name);
#define LOG_TAG "BPLUS CUSTLIB"

/********************Please update this version number accordingly you get the new package**************/
const char *version = "AirForceBT-420.10.00.17-lite (BTE: BCM1200_PI_10.3.20.52-Ampak-1.0.6.1)";
/*******************************************************************************************************/

#ifdef __cplusplus
}
#endif

#endif
